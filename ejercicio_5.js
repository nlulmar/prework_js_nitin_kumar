var texto = "marvel mola!";

/*Y ahora recorre la cadena de texto para imprimir por consola "m-a-r-v-e-l -m-o-l- a-!". 
Aprovéchate de los bucles y las funciones length - charAt()*/

var letras = "" //Crear un str vacío.

for (var i=0; i<texto.length; i++) {
    if(texto.charAt(i)!= " ") { //Eliminar espacios
        if(i==texto.length-1) { //No añadir guión después de la última posición.
            letras = letras.concat(texto.charAt(i)); 
        } else letras = letras.concat(texto.charAt(i),"-");//Añadir cada letra al str seguida de un guión.
    } else continue;
};

console.log(letras)

//Ojo, el método .concat() no añade elementos a la variable como .push() sino que crea una nueva.


/*Primer intento, con array. Mejor con str.

var letras = "" //Crear un array vacío.

for (var i=0; i<texto.length; i++) {
    if(texto.charAt(i)!= " ") { //Eliminar espacios
        if(i==texto.length-1) { //No añadir guión después de la última posición.
            letras.push(texto.charAt(i)); 
        } else letras.push(texto.charAt(i),"-");//Añadir cada letra al array seguida de un guión.
    } else continue;
};

console.log(letras.toString()) //Imprimirlo como cadena de texto para que salga todo en la misma línea.*/