var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];

//Convierto las letras a minúscula ya que, si no, da error si no se introduce el valor en mayúscula.
var letrasMin = [];

for (var i=0; i<letras.length; i++) {
    letrasMin.push(letras[i].toLowerCase());
};

//Comprobar DNI.
var dniNum = window.prompt("Introduzca su n\u00FAmero de DNI (SIN LETRA).");
var dniLetra = window.prompt("Introduzca la letra de su DNI.");

if (dniNum < 0 || dniNum > 99999999) {

    console.log("Su número de DNI no es correcto, pruebe de nuevo.");

} else {

    if(letras[dniNum%23] === dniLetra || letrasMin[dniNum%23] === dniLetra ) {        
        console.log("DNI correcto");

    }else{
        console.log("DNI incorrecto");
    }

}
