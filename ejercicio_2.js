//Función para comprobar el tamaño de la rueda de los juguetes.
function ruedaJuguete (diametro) {
    if(diametro <= 10) {
        console.log("es una rueda para un juguete pequeño");
    } else if(diametro > 20) {
        console.log("es una rueda para un juguete grande");
    } else {
        console.log("es una rueda para un juguete mediano");
    }
};

//Bucle para comprobar distintos números.

diametro = [5,14,18,25,99,256,0.5,16, 10.01];

for (var i = 0; i < diametro.length; i++) {
    console.log("La rueda mide ",diametro[i]," mm. entonces ");
    ruedaJuguete(diametro[i]);
};